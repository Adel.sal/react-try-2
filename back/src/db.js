import sqlite from 'sqlite'
import SQL from 'sql-template-strings';

const test = async () => {

  const db = await sqlite.open('./db.sqlite');

  // await db.run(`CREATE TABLE thermometers (thermometers_id INTEGER PRIMARY KEY AUTOINCREMENT, Temperature_type TEXT NOT NULL, thermometers_types text NOT NULL, Used text NOT NULL);`);

  const stmt = await db.prepare(SQL`INSERT INTO thermometers (Temperature_type, thermometers_types, Used) VALUES (?, ?, ?)`);
  let i = 0;
  while(i<10){
    await stmt.run(`Temperature_type${i}`,`person ${i}`,`person${i}@server.com`);
    i++
  }
  await stmt.finalize();

  const rows = await db.all("SELECT thermometers_id AS id, Temperature_type, thermometers_types, Used FROM thermometers")
  // rows.forEach( ({ id, Temperature_type, thermometers_types, Used  }) => console.log(`[id:${id}] - ${Temperature_type} - ${thermometers_types} - ${Used}`) )
  return(rows)
  console.log(rows)
}

export default { test }



// import sqlite from 'sqlite'
// import SQL from 'sql-template-strings';

// const initializeDatabase = async () => {

//   const db = await sqlite.open('./db.sqlite');
  
//   /**
//    * retrieves the contacts from the database
//    */
//   const getContactsList = async () => {
//     let returnString = ""
//     const rows = await db.all("SELECT thermometers_id AS id, Temperature_type, thermometers_types, Used FROM thermometers")
//     rows.forEach( ({ id, name, email }) => returnString+=`[id:${id}] - ${name} - ${email}` )
//     return returnString
//   }
  
//   const controller = {
//     getContactsList
//   }

//   return controller
// }

// export default initializeDatabase
